using System.Collections;
using UnityEngine;

namespace Bike
{
    public sealed class BikeBoost : MonoBehaviour
    {
        public delegate SO_BikeSetting GetScriptableObject();
        public event GetScriptableObject OnGetBikeSettings;

        public delegate void BoostUsages(int availableBoostUses);
        public event BoostUsages OnUpdateBoostUses;

        public bool isBoosting, isOnCooldown;
        public float evaluation;
        private float boostStartTime, boostEndTime, elapsedTimeByBoost;
        private float boostDuration,  boostCooldown;
        private AnimationCurve boostAcceleration;

        private int maxBoostUses, availableBoostUses;

        private Coroutine cooldownRoutine;

        public bool IsBoosting() {  return isBoosting; }   
        public float Evaluation() {   return evaluation; }

#if UNITY_EDITOR
        public float cooldownTimeLeft;
#endif

        private void Start()
        {
            SetBikeSettingsFromScriptableObject();
            isBoosting = isOnCooldown = false;
            availableBoostUses = maxBoostUses;
            OnUpdateBoostUses?.Invoke(availableBoostUses);
        }

        private void SetBikeSettingsFromScriptableObject()
        { 
            SO_BikeSetting so = OnGetBikeSettings?.Invoke();

            boostDuration = so.BoostDuration;
            boostAcceleration = so.BoostAccelerationCurve;
            boostCooldown = so.BoostCooldown;
            maxBoostUses = so.BoostUses;            
        }

        private void Update()
        {
#if UNITY_EDITOR
            SetBikeSettingsFromScriptableObject();
#endif

            if (isBoosting)
            {
                float elapsedTime = Time.time - boostStartTime;

                elapsedTimeByBoost = elapsedTime / boostDuration;
                evaluation = boostAcceleration.Evaluate(elapsedTimeByBoost);
            }

            if (isBoosting && Time.time >= boostEndTime)
            {
                EndBoost();
            }
        }

        public void HandleResetBoostUsages()
        {
            availableBoostUses = maxBoostUses;
        }

        public void HandleOnBoost()
        {
            if ((isBoosting || isOnCooldown) || availableBoostUses < 1)
                return;

            isBoosting = true;
            availableBoostUses--;
            OnUpdateBoostUses?.Invoke(availableBoostUses);
            boostStartTime = Time.time;
            boostEndTime = boostStartTime + boostDuration;
        }

        private void EndBoost()
        {
            evaluation = 0;
            isBoosting = false;

            if (cooldownRoutine != null)
            {
                StopCoroutine(cooldownRoutine);
            }

            cooldownRoutine = StartCoroutine(CooldownRoutine());
        }

        private IEnumerator CooldownRoutine()
        {
            isOnCooldown = true;

            yield return null;
            float time = boostCooldown;

            while(time > 0)
            {
                time -= Time.deltaTime;

#if UNITY_EDITOR
                cooldownTimeLeft = time;
#endif

                yield return null;
            }

            isOnCooldown = false;
        }
    }
}
