using UnityEngine;
using UnityEngine.InputSystem;

namespace Bike
{
    public sealed class InputHandler_Player : MonoBehaviour, IInputHandler
    {
        public delegate void InputHandler(Vector2 inputVector);
        public event InputHandler OnSetInput;

        public delegate void BoostHandler();
        public event BoostHandler OnBoost;

        private Vector2 inputVector = Vector2.zero;

        // Components
        //private BikeController bikeController;
        private PlayerInputActions playerInputActions;
        private PlayerAttackHandler attackHandler;

        public void Initialize()
        {
            playerInputActions = new PlayerInputActions();
            attackHandler = GetComponent<PlayerAttackHandler>();

            playerInputActions.DrivingControls.Enable();
            playerInputActions.DrivingControls.Accelerate.performed += Accelerate;
            playerInputActions.DrivingControls.Accelerate.canceled += Accelerate;
            playerInputActions.DrivingControls.Break.performed += Break;
            playerInputActions.DrivingControls.Break.canceled += Break;

            playerInputActions.DrivingControls.Boost.performed += Boost;

            playerInputActions.DrivingControls.Attack.performed += Attack;

            playerInputActions.DrivingControls.Rotate.performed += Steer;
            playerInputActions.DrivingControls.Rotate.canceled += Steer;
        }

        private void Update()
        {
            OnSetInput?.Invoke(inputVector);
        }

        public void Accelerate(InputAction.CallbackContext context)
        {
            //TODO:Checkar se o calculo está ok ou se o input precisa de retrabalho.
            if (context.performed)
            {
                // Accelerate
                inputVector.y = 1;
            }
            else if (context.canceled)
            {
                inputVector.y = 0;
            }
        }

        public void Break(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                // Brake
                inputVector.y = -1;
            }
            else if (context.canceled)
            {
                
                inputVector.y = 0;
            }
        }

        public void Steer(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                // Steer 
                inputVector.x = playerInputActions.DrivingControls.Rotate.ReadValue<float>();
            }
            else if (context.canceled)
            {
                // Stop steering
                inputVector.x = 0;
            }
        }

        public void Boost(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                OnBoost?.Invoke();
            }
        }

        public void Attack(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                attackHandler.StartAttack();
            }
        }

        private void OnDisable()
        {
            playerInputActions.DrivingControls.Disable();
        }
    }
}
