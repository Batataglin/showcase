using System;
using UnityEngine;

namespace Bike
{
    public sealed class BikeManager : MonoBehaviour
    {
        public event Action<int> OnUpdateBoostUses;

        [SerializeField] private RespawnerHandler respawnerHandler;
        [SerializeField] private RaceStatus raceStatus;
        [SerializeField] private ArcadeBikeController arcadeBikeController;
        [SerializeField] private BikeBoost bikeBoostController;

#if UNITY_EDITOR
        [SerializeField] private bool forceInitialize;

        private void Start()
        {
            if(forceInitialize)
            {
                InitializeBikeHandler();
            }
        }
#endif

        private void Awake()
        {
            arcadeBikeController.IsBikeBoosting += bikeBoostController.IsBoosting;
            arcadeBikeController.Evaluation += bikeBoostController.Evaluation;

            respawnerHandler.OnDeath += arcadeBikeController.HandleDeath;
            respawnerHandler.OnRespawn += arcadeBikeController.HandleRespawn;
            respawnerHandler.OnRespawned += arcadeBikeController.EnableBikeInput;

            bikeBoostController.OnGetBikeSettings += arcadeBikeController.GetBikeSettings;



            if (TryGetComponent(out CarDriverAI CarDriverAI))
            {
                //CarDriverAI.OnGetTargetDistace += bikeController.GetTargetDistance;
                //CarDriverAI.OnSetInput += arcadeBikeController.SetInputVector;
            }

            if (TryGetComponent(out InputHandler_Player player))
            {
                player.OnSetInput += arcadeBikeController.SetInputVector;
                player.OnBoost += bikeBoostController.HandleOnBoost;
                bikeBoostController.OnUpdateBoostUses += HandleUpdateBoostUses;
            }
        }

        public void HandleEnableBikeInput()
        {
            arcadeBikeController.EnableBikeInput();
        }

        public void HandleDisableBikeInput()
        {
            arcadeBikeController.DisableBikeInput();
        }

        public void HandleThroughCheckpoint(int checkpointIndex)
        {
            raceStatus.SetCheckpointIndex(checkpointIndex);
        }

        public void HandleLapDone()
        {
            raceStatus.LapDone();
            bikeBoostController.HandleResetBoostUsages();
        }

        public void HandleUpdateBoostUses(int boostsAvailable)
        {
            // Raise the event to notify UIManager about boost count updates
            OnUpdateBoostUses?.Invoke(boostsAvailable);
        }

        public void InitializeBikeHandler()
        {
            if (TryGetComponent(out IInputHandler inputHandler))
            {
                inputHandler.Initialize();
            }
        }
    }
}
