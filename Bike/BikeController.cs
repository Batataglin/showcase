using Assets.Interface.Bike;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem.HID;

namespace Bike
{
    [Obsolete]
    public sealed class BikeController : MonoBehaviour, IBikeController
    {
        [Header("Components")]
        [SerializeField] private SO_BikeSetting bikeSetting;
        [SerializeField] private Rigidbody sphereRigidbody;
        [SerializeField] private Rigidbody bikeBody;
        [SerializeField] private Transform bikeModel; // Use this transform as a root for the meshses

        [Header("Ground")]
        [SerializeField] private float groundCheckDistance = 0.2f; // The distance to cast the ray for ground detection
        [SerializeField, Range(0.01f, 1f)] private float bikeXTiltIncrement = 0.09f;
        [SerializeField] private LayerMask groundLayer; // The layer(s) representing the ground

        [Header("Handle bar")]
        [SerializeField] private float handleRotationValue = 30f;
        [SerializeField] private float handleRotationSpeed = 0.2f;
        [SerializeField] private Transform handleBars;

        public bool isGrounded;
        private float rayLenght;
        private RaycastHit hit;

        public float currentVelocityOffset;

        private float accelerationIput = 0;
        private float steeringInput = 0;
        private float currentSpeed;

        public bool isInputEnabled;

        private Coroutine ghostCoroutine;
        private Vector3 velocity;

        public float GetCurrentSpeed { get { return currentSpeed; } }

        public float GetTargetDistance()
        {
            //return bikeSetting.ReachedTargetDistance;
            return 0;
        }

        private void Awake()
        {
            isInputEnabled = false;
        }

        private void Start()
        {
            sphereRigidbody.transform.parent = null;
            rayLenght = sphereRigidbody.GetComponent<SphereCollider>().radius + groundCheckDistance;
        }

        private void Update()
        {
            transform.position = sphereRigidbody.transform.position;
            bikeBody.MoveRotation(transform.rotation);

            velocity = bikeBody.transform.InverseTransformDirection(bikeBody.velocity);
            currentVelocityOffset = velocity.z;// / bikeSetting.TopSpeed;
        }

        private void FixedUpdate()
        {
            Movement();
        }

        /// <summary>
        /// Method to receive the values from the player/IA input, which controls the bike movement
        /// </summary>
        /// <param name="inputVector">X value for steering input. Y value for acceleration input</param>
        public void SetInputVector(Vector2 inputVector)
        {
            steeringInput = inputVector.x;
            accelerationIput = inputVector.y;
        }

        /// <summary>
        /// Stop movement when die
        /// </summary>
        public void HandleDeath()
        {
            isInputEnabled = false;

            sphereRigidbody.velocity = Vector3.zero;
        }

        /// <summary>
        /// Set the player's position and rotation to the custom respawn position
        /// </summary>
        /// <param name="respawnPosition">Respawn position from the last checkpoint</param>
        /// <param name="respawnRotation">Respawn rotation from the last checkpoint</param>
        public void HandleRespawn(Vector3 respawnPosition, Quaternion respawnRotation)
        {
            if (ghostCoroutine != null)
            {
                StopCoroutine(ghostCoroutine);

            }
            ghostCoroutine = StartCoroutine(GhostMode());



            transform.SetPositionAndRotation(respawnPosition, respawnRotation);
            isInputEnabled = true;
        }

        public void EnableBikeInput()
        {
            isInputEnabled = true;
        }

        public void DisableBikeInput()
        {
            isInputEnabled = false;
        }

        private IEnumerator GhostMode()
        {
            //TODO Disable collision with another bikes
            //TODO Made the bike untargetable and unvunerable to attacks

            MeshRenderer[] meshes = bikeModel.GetComponentsInChildren<MeshRenderer>();

            float ghostTime = 1.5f; // time in seconds
            while (ghostTime > 0)
            {
                foreach (MeshRenderer mesh in meshes)
                {
                    ToggleMeshVisibility(mesh);
                }
                ghostTime -= Time.fixedDeltaTime;

                yield return null;
            }

            // Enforce that the meshes are active by the end of the ghost mode
            foreach (MeshRenderer mesh in meshes)
            {
                mesh.enabled = true;
            }
        }

        private void ToggleMeshVisibility(MeshRenderer mesh)
        {
            mesh.enabled = !mesh.enabled;
        }

        public void Acceleration()
        {
            sphereRigidbody.velocity = Vector3.Lerp(sphereRigidbody.velocity, bikeSetting.TopSpeed * accelerationIput * transform.forward, Time.fixedDeltaTime * bikeSetting.Acceleration);
        }

        public void Rotation()
        {
            transform.Rotate(0, steeringInput * accelerationIput * bikeSetting.Turn * Time.fixedDeltaTime, 0, Space.World);

            // Visuals
            handleBars.localRotation = Quaternion.Slerp(handleBars.localRotation, 
                Quaternion.Euler(handleBars.localRotation.eulerAngles.x, handleRotationValue * steeringInput, handleBars.localRotation.eulerAngles.z), handleRotationSpeed);

        }

        public void Break()
        {
            
            {
                sphereRigidbody.velocity *= bikeSetting.BrakeFactor / 10f;
            }
        }

        public void Movement()
        {
            if (IsGrounded())
            {
                if (accelerationIput > 0)
                    Acceleration();
                
                Rotation();
                Break();
            }
            else
            {
                Gravity();
            }

            Tilt();
        }

        private void Gravity()
        {
            sphereRigidbody.AddForce(bikeSetting.BikeMass * Vector3.down, ForceMode.Acceleration);
        }

        private void Tilt()
        {
            float xRotation = (Quaternion.FromToRotation(bikeBody.transform.up, hit.normal) * bikeBody.transform.rotation).eulerAngles.x;
            float zRotation = 0;

            //if(currentVelocityOffset > bikeSetting.MinimumSpeedToTurn)
            {
                zRotation = -45 * steeringInput * currentVelocityOffset;
            }

            Quaternion targetRotation = Quaternion.Slerp(bikeBody.transform.rotation, Quaternion.Euler(xRotation, transform.eulerAngles.y, zRotation), bikeXTiltIncrement);

            Quaternion newRotation = Quaternion.Euler(targetRotation.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);

            Debug.Log(targetRotation);

            bikeBody.MoveRotation(newRotation);
        }


        public LayerMask GetGroundLayer()
        {
            return groundLayer;
        }


        public bool IsGrounded()
        {
            isGrounded = Physics.Raycast(sphereRigidbody.position, Vector3.down, out hit, rayLenght, groundLayer);

            return isGrounded;
        }
    }
}
