using UnityEngine;

[CreateAssetMenu(fileName = "Bike Setting", menuName = "ScriptableObjects/Bike Setting", order = 1)]
public sealed class SO_BikeSetting : ScriptableObject
{
    [Header("Bike")]    
    [SerializeField, Tooltip("Biker's portrait")]
    private Sprite bikerPortrait;

    [SerializeField, Tooltip("Bike boost settings")]
    private SO_BikeBoost bikeBoost;
    
    [SerializeField, Range(1f, 100f), Tooltip("Bike top speed")] 
    private float topSpeed = 15.0f;

    [SerializeField, Range(1f, 100f), Tooltip("Bike top REVERSE speed")]
    private float reverseSpeed = 15.0f;

    [SerializeField, Range(1f, 100f), Tooltip("Bike acceleration force applied when accelerate")] 
    private float acceleration = 30.0f;
    
    [SerializeField, Range(1f, 100f), Tooltip("Brake applied when stop accelerate, it increase the rigidbody drag")] 
    private float brakeFactor = 30f;
    
    [SerializeField, Range(1f, 1000f), Tooltip("Bike steer strenght. Higher values, more turning")]
    private float turn = 100f;
    
    [SerializeField, Range(0f, 100f), Tooltip("Minimum speed before allow the turn factor")] 
    private float minimumSpeedTurn = 10f;
      
    [SerializeField, Range(0f, 100f), Tooltip("The force applied to the bike when it is not grounded")]
    private float bikeGravity = 2f;

    [SerializeField, Tooltip("Curve of the friction applied to the wheels over speed")]
    private AnimationCurve frictionCurve;
   
    [SerializeField, Tooltip("Curve of the bike turn over speed")]
    private AnimationCurve turnCurve;
   
    [SerializeField, Tooltip("Curve of the bike lean over speed")]
    private AnimationCurve leanCurve;
   
    [SerializeField, Tooltip("Physic material which control the wheel friction")]
    private PhysicMaterial frictionMaterial;
    
    [Space, Header("Audio settings")]
    [SerializeField, Tooltip("Source audio for the skid marks")]
    private AudioClip skidSound;
    
    [SerializeField, Tooltip("Source audio for the bike engine")]
    private AudioClip engineSound;
    
    [SerializeField, Range(1, 5), Tooltip("Minimum pitch for the engine")]
    private float minPitch;
    
    [SerializeField, Range(1, 5), Tooltip("Maximum pitch for the engine")]
    private float maxPitch;


    [Space, Header("Attack")]
    [SerializeField, Range(0, 50)] private float thrustForce;
    [SerializeField, Range(0, 1)] private float slopeThrustForce;
    [SerializeField, Range(0.5f, 2f)] private float attackDuration;
    [SerializeField, Range(0, 1)] private float nonGroundedThrustForce;
    [SerializeField, Range(0, 10)] private float cooldown;

    public Sprite Sprite { get => bikerPortrait; }
    
    // BIKE SETTING    
    public float TopSpeed { get => topSpeed; }
    public float ReverseSpeed { get => reverseSpeed; }
    public float Acceleration { get => acceleration; }
    public float BrakeFactor { get => brakeFactor; }
    public float Turn { get => turn; }
    public float MinimumSpeedToTurn { get => minimumSpeedTurn; }
    public float BikeMass { get => bikeGravity; }

    // TURN
    public AnimationCurve FrictionCurve { get => frictionCurve; }
    public AnimationCurve TurnCurve { get => turnCurve; }
    public AnimationCurve LeanCurve { get => leanCurve; }
    public PhysicMaterial FrictionMaterial { get => frictionMaterial; }

    // AUDIO
    public AudioClip SkidSound { get => skidSound; }
    public AudioClip EngineSound { get => engineSound; }
    public float MinPitch { get => minPitch; }
    public float MaxPitch { get => maxPitch; }

    // BOOST
    public AnimationCurve BoostAccelerationCurve { get => bikeBoost.accelerationCurve; }
    public float BoostDuration { get => bikeBoost.duration; }
    public float BoostCooldown { get => bikeBoost.cooldown; }
    public int BoostUses { get => bikeBoost.uses; }

    // ATTACK
    public float ThrustForce { get => thrustForce; }
    public float SlopeThrustForce { get => slopeThrustForce; }
    public float AttackDuration { get => attackDuration; }
    public float NonGroundedThrustForce { get => nonGroundedThrustForce; }
    public float Cooldown { get => cooldown; }
}
