﻿namespace Assets.Interface.Bike
{
    interface IBikeController
    {
        void Movement();
        void Acceleration();
        void Rotation();
        void Break();
    }
}
