using UnityEngine;

[CreateAssetMenu(fileName = "Bike Boost", menuName = "ScriptableObjects/Bike Boost", order = 2)]
public sealed class SO_BikeBoost : ScriptableObject
{
    public AnimationCurve accelerationCurve;
    [SerializeField, Range(0f,10f), Tooltip("Duration (in seconds) of the boost")]
    public float duration;
    [SerializeField, Range(0f,60f), Tooltip("Cooldown (in seconds) of the boost")]
    public float cooldown;
    [SerializeField, Tooltip("How many boost the bike will have")]
    public int uses;
}
