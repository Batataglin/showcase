using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SO_BikeSetting))]
public class BikeEditor : Editor
{
    SO_BikeSetting sO_BikeSetting;

    private readonly string defaultSpritePath = "Assets/Graphics/Inspector/Spriteless.png";

    private void OnEnable()
    {
        sO_BikeSetting = target as SO_BikeSetting;
    }

    public override void OnInspectorGUI()
    {
        if (sO_BikeSetting.Sprite == null)
        {
            Texture2D texture = AssetDatabase.LoadAssetAtPath<Texture2D>(defaultSpritePath);

            if (texture != null)
            {
                GUILayout.Label("", GUILayout.Height(64), GUILayout.Width(64));
                GUI.DrawTexture(GUILayoutUtility.GetLastRect(), texture);
            }
            else
            {
                EditorGUILayout.HelpBox("Default sprite not found at path: " + defaultSpritePath, MessageType.Error);
            }
        }
        else
        {
            Texture2D texture = AssetPreview.GetAssetPreview(sO_BikeSetting.Sprite);
            GUILayout.Label("", GUILayout.Height(64), GUILayout.Width(64));
            GUI.DrawTexture(GUILayoutUtility.GetLastRect(), texture);
        }

        base.OnInspectorGUI();
    }
}
