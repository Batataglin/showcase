using UnityEngine;

namespace Bike
{
    public sealed class ArcadeBikeController : MonoBehaviour
    {
        public enum groundCheck { rayCast, sphereCaste };
        public enum MovementMode { Velocity, AngularVelocity };

        [SerializeField] private SO_BikeSetting bikeSetting;

        [SerializeField] private MovementMode movementMode;
        [SerializeField] private groundCheck GroundCheck;
        [SerializeField] private LayerMask drivableSurface;
        [SerializeField] private Rigidbody rb, carBody;

        [Header("Audio")]
        [SerializeField] private AudioSource skidSound;
        [SerializeField] private AudioSource engineSound;

        [Header("Visuals")]
        [SerializeField] private Transform bodyMesh;
        [SerializeField] private Transform handle;
        [SerializeField] private Transform[] Wheels = new Transform[2];

        // TODO: Remove from here!?
        [Range(-70, 70)] public float BodyTilt;
        public float skidWidth;

        // Local vairables
        public bool isGrounded, isInputEnabled;
        private float radius;
        private float  steeringInput, accelerationInput;
        public float maxSpeed;
        private float reverseSpeed, accelaration, turn, minimumSpeedToTurn;
        private float minPitch, maxPitch;
        private AnimationCurve frictionCurve, turnCurve, leanCurve;
        private PhysicMaterial frictionMaterial;
        private RaycastHit hit;
        private Vector3 origin;
        private Vector3 carVelocity;

#if UNITY_EDITOR
        public float velocityNormalized;

        float tempSpeed = 0;
#endif

        public bool IsGrounded { get => isGrounded; }
        public float MaxSpeed { get => maxSpeed; }
        public Vector3 VehicleVelocity { get => carVelocity; }
        public Transform Handle { set => handle = value; }

        public SO_BikeSetting GetBikeSettings() { return bikeSetting; }

        private void SetBikeSettingsFromScriptableObject()
        {
            maxSpeed = bikeSetting.TopSpeed;
            reverseSpeed = bikeSetting.ReverseSpeed;
            accelaration = bikeSetting.Acceleration;
            turn = bikeSetting.Turn;
            minimumSpeedToTurn = bikeSetting.MinimumSpeedToTurn;
            frictionCurve = bikeSetting.FrictionCurve;
            turnCurve = bikeSetting.TurnCurve;
            leanCurve = bikeSetting.LeanCurve;
            frictionMaterial = bikeSetting.FrictionMaterial;

            skidSound.clip = bikeSetting.SkidSound;
            engineSound.clip = bikeSetting.EngineSound;
            minPitch = bikeSetting.MinPitch;
            maxPitch = bikeSetting.MaxPitch;
        }

        private void Awake()
        {
            SetBikeSettingsFromScriptableObject();
        }

        private void Start()
        {
            radius = rb.GetComponent<SphereCollider>().radius;
            if (movementMode == MovementMode.AngularVelocity)
            {
                Physics.defaultMaxAngularSpeed = 150;
            }
            rb.centerOfMass = Vector3.zero;
        }

        private void Update()
        {
#if UNITY_EDITOR
            SetBikeSettingsFromScriptableObject();
            velocityNormalized = carVelocity.magnitude;
#endif

            Visuals();
            AudioManager();
        }


        private void FixedUpdate()
        {
            isGrounded = Grounded();

            carVelocity = carBody.transform.InverseTransformDirection(carBody.velocity);

            if (Mathf.Abs(carVelocity.x) > 0)
            {
                //changes friction according to sideways speed of car
                frictionMaterial.dynamicFriction = frictionCurve.Evaluate(Mathf.Abs(carVelocity.x / 100));
            }

            if (isGrounded && isInputEnabled)
            {
                Turn();
                Break();
                Acceleration();

                //body tilt
                carBody.MoveRotation(Quaternion.Slerp(carBody.rotation, Quaternion.FromToRotation(carBody.transform.up, hit.normal) * carBody.transform.rotation, 0.09f));
            }
            else
            {
                carBody.MoveRotation(Quaternion.Slerp(carBody.rotation, Quaternion.FromToRotation(carBody.transform.up, Vector3.up) * carBody.transform.rotation, 0.02f));
            }

        }

        /// <summary>
        /// Method to receive the values from the player/IA input, which controls the bike movement
        /// </summary>
        /// <param name="inputVector">X value for steering input. Y value for acceleration input</param>
        public void SetInputVector(Vector2 inputVector)
        {
            steeringInput = inputVector.x;
            accelerationInput = inputVector.y;
        }

        /// <summary>
        /// Stop movement when die
        /// </summary>
        public void HandleDeath()
        {
            DisableBikeInput();

            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            carBody.velocity = Vector3.zero;
            carBody.angularVelocity = Vector3.zero;
            carVelocity = Vector3.zero;
        }

        /// <summary>
        /// Set the player's position and rotation to the custom respawn position
        /// </summary>
        /// <param name="respawnPosition">Respawn position from the last checkpoint</param>
        /// <param name="respawnRotation">Respawn rotation from the last checkpoint</param>
        public void HandleRespawn(Vector3 respawnPosition, Quaternion respawnRotation)
        {
            transform.position = new Vector3(respawnPosition.x, respawnPosition.y + 3f, respawnPosition.z);
            transform.rotation = respawnRotation;
        }

        public void EnableBikeInput()
        {
            isInputEnabled = true;
        }

        public void DisableBikeInput()
        {
            isInputEnabled = false;
        }

        public delegate bool Boosting();
        public event Boosting IsBikeBoosting;
        public delegate float BoostEvaluation();
        public event BoostEvaluation Evaluation;

        private void Acceleration()
        {
            //accelaration logic
            const float minInputThreshold = 0.1f;
            float targetSpeed = accelerationInput * maxSpeed;

            float targetAcceleration = accelaration;

            if(IsBikeBoosting.Invoke())
            {
                targetSpeed = accelerationInput * (maxSpeed + Evaluation.Invoke());
            }

            if (movementMode == MovementMode.AngularVelocity)
            {
                if (Mathf.Abs(accelerationInput) > minInputThreshold)
                {
                    rb.angularVelocity = Vector3.Lerp(rb.angularVelocity, targetSpeed * carBody.transform.right / radius, targetAcceleration * Time.fixedDeltaTime);
                }
            }
            else if (movementMode == MovementMode.Velocity)
            {
                // Determine forward or reverse motion
                Vector3 forwardVector = carBody.transform.forward ;

                // Calculate target velocity
                Vector3 targetVelocity = accelerationInput >= minInputThreshold ? forwardVector * targetSpeed : forwardVector * (accelerationInput * reverseSpeed);

                // Lerp to target velocity
                rb.velocity = Vector3.Lerp(rb.velocity, targetVelocity, targetAcceleration * Time.fixedDeltaTime);

            }
        }

        // Not in use at the moment
        private void Break()
        {
            //brakelogic
            if (Input.GetAxis("Jump") > 0.1f)
            {
                rb.constraints = RigidbodyConstraints.FreezeRotationX;
            }
            else
            {
                rb.constraints = RigidbodyConstraints.None;
            }
        }

        private void Turn()
        {
            //turnlogic
            float sign = Mathf.Sign(carVelocity.z);
            float TurnMultiplyer = turnCurve.Evaluate(carVelocity.magnitude / maxSpeed);
            if (Mathf.Abs(turn) > 0.1f || carVelocity.z > minimumSpeedToTurn)
            {
                carBody.AddTorque(sign * steeringInput * turn * TurnMultiplyer * Vector3.up);
            }
        }

        private void AudioManager()
        {
            engineSound.pitch = Mathf.Lerp(minPitch, maxPitch, Mathf.Abs(carVelocity.z) / maxSpeed);
            if (Mathf.Abs(carVelocity.x) > 10 && isGrounded)
            {
                skidSound.mute = false;
            }
            else
            {
                skidSound.mute = true;
            }
        }

        private void Visuals()
        {
            handle.localRotation = Quaternion.Slerp(handle.localRotation, Quaternion.Euler(handle.localRotation.eulerAngles.x,
                                   20 * steeringInput, handle.localRotation.eulerAngles.z), 0.1f);

            Wheels[0].localRotation = rb.transform.localRotation;
            Wheels[1].localRotation = rb.transform.localRotation;

            //Body
            if (carVelocity.z > 1)
            {
                bodyMesh.localRotation = Quaternion.Slerp(bodyMesh.localRotation, Quaternion.Euler(0,
                                   bodyMesh.localRotation.eulerAngles.y, BodyTilt * steeringInput * leanCurve.Evaluate(carVelocity.z / maxSpeed)), 0.02f);
            }
            else
            {
                bodyMesh.localRotation = Quaternion.Slerp(bodyMesh.localRotation, Quaternion.Euler(0, 0, 0), 0.02f);
            }
        }

        private bool Grounded() //checks for if vehicle is grounded or not
        {
            origin = rb.position + rb.GetComponent<SphereCollider>().radius * Vector3.up;
            var direction = -transform.up;
            var maxdistance = rb.GetComponent<SphereCollider>().radius + 0.2f;

            if (GroundCheck == groundCheck.rayCast)
            {
                if (Physics.Raycast(rb.position, Vector3.down, out hit, maxdistance, drivableSurface))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else if (GroundCheck == groundCheck.sphereCaste)
            {
                if (Physics.SphereCast(origin, radius + 0.1f, direction, out hit, maxdistance, drivableSurface))
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }
            else { return false; }
        }

        private void OnDrawGizmos()
        {
            //debug gizmos
            radius = rb.GetComponent<SphereCollider>().radius;
            float width = 0.02f;
            if (!Application.isPlaying)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireCube(rb.transform.position + ((radius + width) * Vector3.down), new Vector3(2 * radius, 2 * width, 4 * radius));
                if (GetComponent<BoxCollider>())
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawWireCube(transform.position, GetComponent<BoxCollider>().size);
                }
                if (GetComponent<CapsuleCollider>())
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawWireCube(transform.position, GetComponent<CapsuleCollider>().bounds.size);
                }

            }

        }

    }
}
