using UnityEngine;

public sealed class RaceStatus : MonoBehaviour
{
    // Lap
    // Position
    // Lap Time
    // Last Checkpoint

    public int CurrentLap
    {
        get; private set;
    }

    public float LapTimeInSeconds
    {
        get; private set;
    }

    public int CurrentPosition
    {
        get; private set;
    }

    public int CheckpointIndex
    {
        get; private set;
    }

    public void LapDone(float lapTime = 0)
    {
        CurrentLap++;
        LapTimeInSeconds = lapTime;
    }

    public void ResetLapCount()
    {
        // The race always start at lap 1;
        CurrentLap = 1;
    }

    public void UpdateCurrentPosition(int position)
    {
        CurrentPosition = position;
    }

    public void SetCheckpointIndex(int index)
    {
        CheckpointIndex = index;
    }

    public int GetCurrentLap()
    {
        return CurrentLap;
    }
}
